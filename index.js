/**
 * Wrapper for npm/node
 */
'use strict';

/**
 * Country to province abbrev and name.
 * Source paywall\storefront\base\paywall-lib\web\templates\new_credit_card.inc
 *
 * Future: this data is pre-translated, it should NOT be.
 *
 * Future: figure out why we use some non-standard abbrevs (i.e. France) and
 * try to swap in https://raw.githubusercontent.com/jshq/countrydb/master/db.json
 */
exports.countryProvinces = require('./src/lg-provinces.json');

/**
 * Occupations list for selection field
 * TODO: need to support multilingual
 */
exports.occupations = require('./src/lg-occupations.json');
