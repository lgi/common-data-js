/**
 * silly simple build file to create a browser JS file from out data
 */
'use strict';

var fs = require('fs');

var data = require('./index.js');

// http://stackoverflow.com/a/24311711
function mkdirSync(path) {
  try {
    fs.mkdirSync(path);
  } catch(e) {
    if ( e.code != 'EEXIST' ) throw e;
  }
}

var output = 'var lgCommonData = ' + JSON.stringify(data) + ';';
mkdirSync('./dest');
fs.writeFileSync('./dest/common-data.js', output);
